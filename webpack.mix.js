const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'public/css/flaticon.css',
        'public/css/fontawesome.min.css',
        'public/css/lightbox.min.css',
        'public/css/magnific-popup.css',
        'public/css/owl.carousel.min.css',
        'public/css/owl.theme.default.min.css',
        'public/css/toastr.min.css',
    ], 'public/assets/front/css/all.min.css');
