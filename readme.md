# Company Profile Professional V3

## Requirement

Laravel 6.0.\*

PHP 7.2

MySQL

## Installation

1. Upload zip folder to cpnel/localhost
2. Composer update
3. Enter to core folder, edit `.env` file, edit db username, password, db name
4. Key Generate
5. Save

## Feature

1. Multiple home version `static, slider, videos, water, parallax, particle`
2. Responsif and modern style home=frontend and dasboard-admin
3. Multiple Language Support
4. Full edited frontend by admin
5. SEO friendly

## Login

`/admin`

`username: admin`

`password: admin`
